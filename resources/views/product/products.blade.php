<table class="table table-striped">
    <thead>
    <tr>
        <th>Id</th>
        <th>Category name</th>
        <th>Product Name</th>
        <th>Discount in Percentage</th>
        <th>Offered Price</th>
        <th>Original Price</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
    <tr>
        <td>{{$product->id}}</td>
        <td>{{$product->category}}</td>
        <td>{{$product->product}}</td>
        <td>{{$product->discount}}</td>
        <td>{{$product->price}}</td>
        <td>{{$product->original_price}}</td>
    </tr>
    @endForeach
    </tbody>
</table>