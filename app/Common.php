<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use DB;

class Common extends Model
{
    public static function strAlias($str){

       $str = strtolower($str);
       return str_replace(" ","_",$str);
    }

    public static  function DBConnection($shop_id){

        $shop_detail = Shops::find($shop_id);

        if(empty($shop_detail)){
            echo 'shop not found';die;
        }
        $db_name = $shop_detail->db_name;

        DB::Table('shops')->whereid($shop_id)->Increment('requests');

        Config::set("database.connections.mysql", [
            "driver"    => 'mysql',
            "host" => env('DB_HOST'),
            "database" => $db_name,
            "username" => env('DB_USERNAME'),
            "password" => env('DB_PASSWORD'),
            'collation' => 'utf8_unicode_ci',
        ]);
        DB::reconnect('mysql');
    }
}
