<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/shop', 'ShopController');

Route::resource('/shop/{shop_id}/product', 'ProductController');

Route::resource('/shop/{shop_id}/products', 'ProductController');

Route::post('/shop_name', 'ShopController@shopName');
