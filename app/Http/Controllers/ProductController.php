<?php

namespace App\Http\Controllers;

use App\Common;
use App\Products;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($shop_id)
    {
        Common::DBConnection($shop_id);

        $products = DB::table('products')
            ->select('products.*',DB::raw('products.price/(100-products.discount)*100 as original_price'))
            ->get();

        return view('product.products', array('products' => $products));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$shop_id)
    {
        $param = $request->all();

        $validator = Validator::make($param, [
            'category' => "Required",
            'product' => "Required",
            'price' => "Required|integer"
        ]);

        if ($validator->fails()) {
            return \Response::json(array('error' => true, 'message' => $validator->errors()->all()));
        } else {
            // db connection
            Common::DBConnection($shop_id);
            // discounted price count
            $param['price'] =$param['price']-($param['price']*$param['discount']/100);
            // add new product
            Products::create($param);
            return \Response::json(array('success' => true, 'message' => 'Successfully added new product'));
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shop_id,$product_id)
    {
        Common::DBConnection($shop_id);
        $result = DB::table('products')->select('products.*',DB::raw('products.price/(100-products.discount)*100 as price'))->find($product_id);

        return \Response::json(array('success' => true, 'data' => $result));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $shop_id,$product_id)
    {
        $param = $request->all();

        $validator = Validator::make($param, [
            'category' => "Required",
            'product' => "Required",
            'price' => "Required|integer"
        ]);

        if ($validator->fails()) {
            return \Response::json(array('error' => true, 'message' => $validator->errors()->all()));
        } else {
            Common::DBConnection($shop_id);
            $param['price'] =$param['price']-($param['price']*$param['discount']/100);

            $Products = Products::where('id',$product_id)->update($param);

            if($Products ==0){
                return \Response::json(array('error' => true, 'message' => 'resource not found'));
            }

            return \Response::json(array('success' => true, 'message' => 'Successfully update product'));
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($shop_id,$product_id)
    {
        Common::DBConnection($shop_id);
        $Products = Products::where('id',$product_id)->delete();

        if($Products ==0){
            return \Response::json(array('error' => true, 'message' => 'resource not found'));
        }

        return \Response::json(array('success' => true, 'message' => 'Successfully delete product'));
    }
}
