<?php

namespace App\Http\Controllers;

use App\Common;
use App\Products;
use App\Shops;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use DB;
use Illuminate\Support\Facades\Response;
use Validator;

class ShopController extends Controller
{

    public function index()
    {
        $Products = DB::table('shops')->get();
        return Response::json(array('success'=>true,'data'=>$Products));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo 'create';
    }

    private function checkDBName($dbName)
    {

        $checkDb = Shops::where('db_name', $dbName)->first();

        if (!empty($checkDb->db_name)) {
            return $this->checkDBName($dbName . '_1');
        } else {
            return $dbName;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $param = $request->all();

        $validator = Validator::make($param, [
            'name' => "Required"
        ]);

        if ($validator->fails()) {
            return \Response::json(array('error' => true, 'message' => $validator->errors()->all()));
        } else {

        $dbName = Common::strAlias($param['name']);
        $newDBName = $this->checkDBName($dbName);

        // create database
        DB::statement('create database ' . $newDBName);
        $Shops = Shops::create(array('shop_name' => $param['name'], 'db_name' => $newDBName));
        $return = array('shop_id' => $Shops->id, 'shop_name' => $Shops->shop_name, 'db_name' => $Shops->db_name);

        // database connection
        Common::DBConnection($Shops->id);

        DB::statement('CREATE TABLE IF NOT EXISTS `products` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(250) NOT NULL,
  `product` varchar(250) NOT NULL,
  `discount` int(50) NOT NULL DEFAULT "0",
  `price` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1');

        return \Response::json(array('success'=>true,'data'=>$return));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Common::DBConnection($id);

        $products = DB::table('products')->select('id')->get();
        $productName = '<option value="">Select Product</option>';
        foreach($products as $product){
            $productName .= "<option value='$product->id'>$product->id</option>";
         }

        return $productName;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo 'edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo 'update';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo 'destroy';
    }

    public function shopName(){

        $shops = DB::table('shops')->select('id','shop_name')->get();
        $shopName = array(''=>'Select Shop');
//        $shopName[''] = 'Select Shop';
        foreach($shops as $shop){
            $shopName[$shop->id] = $shop->shop_name;
        }
        return Response::json($shopName);
    }
}
