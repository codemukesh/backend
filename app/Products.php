<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = ['category','product','discount','price'];

    protected $table = 'products';


}
